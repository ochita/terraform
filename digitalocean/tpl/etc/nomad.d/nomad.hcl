# Full configuration options can be found at https://www.nomadproject.io/docs/configuration

data_dir = "/opt/nomad/data"
bind_addr = "{{ GetInterfaceIP \"ztyouufc7n\" }}"
leave_on_interrupt = true
leave_on_terminate = true


#addresses {
#
#http = "{{ GetInterfaceIP \"eth0\" }}"
#}

advertise {
  # Defaults to the first private IP address.
     http = "{{ GetInterfaceIP \"ztyouufc7n\" }}"
       }

log_level = "DEBUG"


consul {
  address = "127.0.0.1:8500, Owncloud"
}

server {
  enabled = false
  bootstrap_expect = 1

server_join {
    retry_join = ["10.147.18.147:4648"]
  }

}



client {
  enabled = true
  network_interface = "ztyouufc7n"
  servers = ["10.147.18.147"]

options {
   "docker.privileged.enabled" = "true"
}


}