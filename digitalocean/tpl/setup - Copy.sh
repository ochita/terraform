#!/bin/bash
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
apt-add-repository -y "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
apt-get -y update && sudo apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install consul
apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install nomad
apt -y install docker.io
wget -O wesher https://github.com/costela/wesher/releases/latest/download/wesher-amd64
chmod a+x wesher
mv wesher /usr/local/sbin/
systemctl daemon-reload
systemctl enable wesher
service docker start
service wesher start
sleep 5
service consul start
sleep 5
service nomad start
