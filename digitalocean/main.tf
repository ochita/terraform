terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.5.1"
    }
  }
}


data "digitalocean_ssh_key" "terraform" {
  name = "terraform"
}

variable "do_token" {}
variable "pvt_key" {}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_droplet" "www-1" {
    image = "ubuntu-20-10-x64"
    name = "nomad-server-1"
    region = "AMS3"
    size = "s-1vcpu-1gb"
    private_networking = true
    ssh_keys = [data.digitalocean_ssh_key.terraform.id]

connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }
 
 
  provisioner "file" {
    source      = "tpl/etc/"
    destination = "/etc/"
  }

  provisioner "file" {
    source      = "tpl/setup.sh"
    destination = "/root/setup.sh"
  }


  provisioner "remote-exec" {
    inline = [
      "chmod +x /root/setup.sh",
      "/root/setup.sh"
    ]
  }
}


